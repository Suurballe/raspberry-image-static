#!/usr/bin/env bash

if [ "x" == "x$1" ]; then
	echo "usage $0 <new image name>"
	exit 1
fi

echo "combining to $1"
cat raspberry-latest-img/part?.img > $1
